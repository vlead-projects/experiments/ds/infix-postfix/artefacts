#+TITLE:  Index to Artefacts and Realization Catalog
#+AUTHOR: VLEAD
#+DATE: 
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This is an index to implementation of artefacts and
  realization catalog.

* Number of artefacts : 4
+ 1 Demo Artefact
+ 1 Practice Artefact
+ 2 Exercise Artefacts

* External pictures/videos used : None
* References
** A mapping between different implementations and their roles.
    |------+-----------------------------------+--------------------------------------------------------------------|
    | S.No | Artefacts and Realization Catalog | Role                                                               |
    |------+-----------------------------------+--------------------------------------------------------------------|
    |    1 | [[./runtime/html/demo_html.org][Validation of Expressions]]         | Holds the demo artefact for validation of expressions              |
    |------+-----------------------------------+--------------------------------------------------------------------|
    |    2 | [[./runtime/html/pushandoutput_html.org][Push and Pop Operations Stack]]     | Holds the practice artefact for push operation                     |
    |------+-----------------------------------+--------------------------------------------------------------------|
    |    3 | [[infix_to_postfix_html.org][Infix to Postfix]]                  | Holds the exercise artefact for conversion of infix to postfix     |
    |------+-----------------------------------+--------------------------------------------------------------------|
    |    4 | [[./runtime/html/postfix_eval_html.org][Evaluation of Postfix]]             | Holds the exercise artefact for evaluation of postfix expressions. |
    |------+-----------------------------------+--------------------------------------------------------------------|
    |    5 | [[./runtime/js/realization_catalog.org][Realization Catalog]]               | Holds the realization catalog                                      |
    |------+-----------------------------------+--------------------------------------------------------------------|

** Link to the videos uploaded on YouTube : [[][Infix to Postfix Videos]]

