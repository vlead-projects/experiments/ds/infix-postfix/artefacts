#+TITLE: Whole Conversion (HTML)
#+AUTHOR: Mernedi Venkata Naga Narasimha Ashish
#+DATE: 2019-06-20 Thu
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Whole Conversion= interactive
  artefact(HTML).

* Features of the artefact
+ Artefact provides an idea how to convert infix to postfix expressions.
+ User can see a randomly generated infix expression in the beginning.
+ User can see two input boxes where the user has to enter elements which needed to be pushed onto stack
  and elements which are needed to be seen in output.
+ User can click the =Reset= button to start the experiment again
  at any time with same question.
+ User can click on =Push= button to push the entered element into stack.
+ User can click on =Pop= button to pop the top most element from the stack.
+ User can click on =To Output= to pop the selected element into the output.
+ User can click on =To Ignore= to just remove th etop most element in the stack. 
+ User can click on =New Question= button to start a new experiment with a different question.
+ User can click on =Check= button to check whether the output expression is correct postfix expression 
  for the given infix expression.

* HTML Framework of Simple Infix to Postfix
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
  <title>Infix to Postfix</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
    <link rel="stylesheet" href="../css/infix_to_postfix_styles.css">
  <link rel="stylesheet" href="../css/styles.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/4.4.2/math.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/4.4.2/math.min.js"></script>
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction
#+BEGIN_SRC html
<div id="instructionbox" class="instruction-box">
  <button class="collapsible"> Instructions </button>
  <div class ="content">
    <ul><li>Push the ‘(‘ and operators onto stack and output all numbers. </li>
    <li>If the scanned element is ‘)’, pop and output from stack till an ‘(‘ is encountered. </li>
    <li>Repeat steps 1 and 2, then pop and output the stack till it is empty.</li>
    <li>For popping out elements first click on pop then click on given options to perform the operation.</li></ul>
  </div>
</div>
#+END_SRC

*** Main Artefact
This is the place where the conversion is done.
#+NAME: conversion
#+BEGIN_SRC html
<p id="questionid" class="question"></p>
<div id="infix_to_postfix">
  <div class="column0">
    <input id="pushtostack" placeholder="Type" type="tel" pattern="^[0-9-+\s()]*$" class="text-box">
    <button id="push_tostack_button" class="button-input" type="button">Push</button>
    <p id="push_to_stack_text">Push to Stack</p>
  </div>
  <div class="column0">
    <div class="row1">
      <button id="pop_button" class="button-input" type="button">Pop</button>
    </div>
    <div id="column1">
      <input id="tooutput" class="radio_button" type="radio" value="">
      <p id="tooutputtext">To Output</p>
    </div>
    <div id="column2">
      <input id="toignore" class="radio_button" type="radio" value="">
      <p id="toignoretext">To Ignore</p>
    </div>
    <div class="row1">
      <canvas id="canvas_for_conversion" width="500" height="600"></canvas>
    </div>
  </div>
  <div class="column0">
    <input id="typetooutput" placeholder="Type" type="number" class="text-box">
    <button id="type_tooutput_button" class="button-input" type="button">Output</button>
    <p id="to_output">Type to output</p>
    <p id="output"><span style='font-weight:bold;'>Output:</span></p>
  </div>
</div>
#+END_SRC

*** Controller Elements
This are the controller buttons(New Question, Reset, Check) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: controller-elems
#+BEGIN_SRC html
<div id="comments">
  <p><b>Observations:</b></p>
  <p id="correct" class=""></p>
  <p id="wrong" class=""></p>
  <p id="ins" class=""></p>
</div>
<div id="controller">
  <button type="reset" class="button-input" id="restart_button">Reset</button>
  <button id="question" class="button-input" name="generate_random_infix" type="button">New Question</button>
  <button type="button" class="button-input" id="check_button">Check</button>
  <script src="../js/infix_to_postfix.js"></script>
  <script src="../js/check_postfix.js"></script>
  <script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
</div>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle infix_to_postfix.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body onload="generate()">
    <<instruction>>
    <div class="wrapper">
      <<conversion>>
      <<controller-elems>>
    </div>
  </body>
</html>
#+END_SRC
