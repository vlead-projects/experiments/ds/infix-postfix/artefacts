#+title: Realization catalog 
#+AUTHOR: VLEAD
#+DATE: [2018-05-14 Mon]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
#+NAME: realization-catalog
#+BEGIN_SRC javascript

var realizationCatalog = {
    "Quiz for Pretest" :  {
        "resource_type": "html",
        "url": "/build/code/runtime/html/pretest_quiz_html.html"
    },

    "Quiz on Posttest" :  {
        "resource_type": "html",
        "url": "/build/code/runtime/html/posttest_quiz_html.html"
    },

    "Definition of an Infix Expression":{
        "resource_type":"image",
        "url":"/build/code/static/img/Infix-Traversals-example-pic.gif",
        "height":"30%",
        "width":"75%"
    },

    "Definition of a Postfix Expression":{
        "resource_type":"image",
        "url":"/build/code/static/img/Postfix-Traversals-example-pic.gif",
        "height":"30%",
        "width":"75%"
    },

    "Conversion without Stack":{
        "resource_type":"image",
        "url":"/build/code/static/img/conversion_withoutstack.png",
        "height":"50%",
        "width":"75%"
    },

    "Push operations Example":{
        "resource_type":"image",
        "url":"/build/code/static/img/conversion_withstack_1.png",
        "height":"100%",
        "width":"75%"
    },  

    "Pop Operations Example":{
        "resource_type":"image",
        "url":"/build/code/static/img/conversion_withstack_2.png",
        "height":"100%",
        "width":"75%"
    },

    "Postfix Evaluation Example":{
        "resource_type":"image",
        "url":"/build/code/static/img/postfix_evaluation.png",
        "height":"80%",
        "width":"75%"
    },

    "Validation Demo": {
       "reqs_satisfied": "",
       "resource_type": "html",
       "url": "/build/code/runtime/html/demo.html"
    },
    "Push and Pop Operations Practice": {
       "reqs_satisfied": "",
       "resource_type": "html",
       "url": "/build/code/runtime/html/pushandoutput.html"
    },
    "Conversion Operations Exercise":{
        "reqs_satisfied": "",
        "resource_type": "html",
        "url": "/build/code/runtime/html/infix_to_postfix.html"
    },
    "Exercise for Postfix Evaluation":{
        "reqs_satisfied": "",
        "resource_type": "html",
        "url": "/build/code/runtime/html/postfix_eval.html"
    },

    
    "Infix and Postfix Introduction": {
        "resource_type": "video",
        "url": "https://www.youtube.com/embed/83RGJ-ESy-s"
    },

    "Basics of Infix and Postfix": {
        "resource_type": "video",
        "url": "https://www.youtube.com/embed/g7YM1tjT1D8"
    },

    "Infix to Postfix Algorithm": {
        "resource_type": "video",
        "url": "https://www.youtube.com/embed/tuuUFBbXu_E"
    }
}

#+END_SRC

* Tangle
#+BEGIN_SRC javascript :tangle realization.js  :eval no :noweb yes
<<realization-catalog>>
#+END_SRC
